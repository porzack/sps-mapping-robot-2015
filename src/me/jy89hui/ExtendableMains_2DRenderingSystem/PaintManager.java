package me.jy89hui.ExtendableMains_2DRenderingSystem;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.PaintClass;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;

public class PaintManager extends JPanel implements PaintClass, Runnable {
	private float combinedTimes = 0;
	private int timesPrinted = 0;
	private GraphicsMain graphics;
	public void load(GraphicsMain gm){
		this.graphics=gm;
	}
	@Override
	public void paintComponent(Graphics g1) {
		if (GraphicsMain.canBePainted > 0) {
			GraphicsMain.beginTimeMeasurement(10);
			this.timesPrinted++;
			GraphicsMain.canBePainted--;
			PrintComponents(g1);
			g1.setColor(new Color(10, 10, 10));
			g1.fillRect(5, 4, 80, 15);
			g1.setColor(new Color(255, 100, 52));
			float timeTaken = GraphicsMain.endTimeMeasurement(10, false);
			float currentFPS = 1000 / (timeTaken + GraphicsMain.refreshRate);
			g1.setFont(new Font("Times New Roman", Font.BOLD, 15));
			g1.drawString("" + currentFPS, 5, 15);
			this.combinedTimes = this.combinedTimes + timeTaken;
			GraphicsMain.avgPrintTime = this.combinedTimes / this.timesPrinted;
		}
		return;

	}

	@Override
	public void PrintComponents(Graphics g) {
		ArrayList<SmartComponent> Comps = new ArrayList<SmartComponent>();
		Comps = this.graphics.RealComponents;
		for (int x = 0; x < Comps.size(); x++) {
			Comps.get(x).PrintComp(g);
		}
	}

	@Override
	public void run() {
	}

}
