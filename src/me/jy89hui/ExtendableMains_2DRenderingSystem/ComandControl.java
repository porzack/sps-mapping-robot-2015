package me.jy89hui.ExtendableMains_2DRenderingSystem;

import java.util.ArrayList;

import me.jy89hui.AMain.Main2D;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.ManagingSystems_2DRenderingSystem.CommandManager;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class ComandControl extends CommandManager {
	private static ArrayList<SmartComponent> comps = Main2D.RealComponents;

	public void Load() {
		comps = Main2D.main.RealComponents;
	}

	public void Manage(Command c) {

		logger.silentlog("Command Run: " + c.getFullCommand());
		Load();
		BasicManage(c, Main2D.main);
		c.Handled();
		boolean handled = false; // This is just if the command has been given
									// to the objects
		for (int x = 0; x < comps.size(); x++) {
			if (c.getArgs().length > 2) {
				if (c.getCmd().equalsIgnoreCase(comps.get(x).getName())) {
					comps.get(x).onCommand(c);
				}
			}

		}
		if (!c.isHandled()) {
			logger.warning("There has been a problem handing that command. ");
			logger.warning("Command: [ " + c.getFullCommand() + " ]");
		}
	}
}
