package me.jy89hui.LoadMapStuff;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Action;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartButton;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartLog;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartTextArea;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartTextArea.typeOfTextArea;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;
import me.jy89hui.MappingStuff.Point;
import me.jy89hui.MappingStuff.ServerClass;

public class Reader extends SmartComponent {
	private ServerMain mainclass= ServerMain.main;
	SmartTextArea text;
	SmartTextArea input;
	SmartButton enter;
	private double progress = 0.0;
	SmartTextArea progressValue;

	public Reader(int x, int y, int sx, int sy) {
		this.x = x;
		this.y = y;
		this.sx = sx;
		this.sy = sy;
		this.text = new SmartTextArea(x + (sx / 2) - 250, y + (sy / 2) - 180,
				500, 50, mainclass);
		this.progressValue = new SmartTextArea(x + (sx / 2) - 250, y + (sy / 2)
				+ 50, 500, 50, mainclass);
		this.input = new SmartTextArea(x + (sx / 2) - 250, y + (sy / 2), 500,
				50, mainclass);
		this.enter = new SmartButton(x + (sx / 2) - 75, y + (sy / 2) + 180,
				150, 50, mainclass);
		this.enter.setText(new SmartLog("Load", new Color(21, 61, 17)));
		this.text.textAreaType = typeOfTextArea.PlainText;
		this.progressValue.textAreaType = typeOfTextArea.PlainText;
		ArrayList<SmartLog> listing = new ArrayList<SmartLog>();
		listing.add(new SmartLog(
				"Please type the name of the file you wish to read.",
				new Color(52, 121, 217), new Font("Chalkboard", Font.BOLD, 30)));
		listing.add(new SmartLog("EX: myRoom.txt", new Color(52, 121, 217),
				new Font("Chalkboard", Font.BOLD, 30)));
		this.text.setTextSL(listing);
		this.progressValue.setTextSL("");
		this.text.setShowingBackground(false);
		this.progressValue.setShowingBackground(false);
		this.input.textAreaType = typeOfTextArea.TextInput;
		enter.enableAction(new Action() {
			@Override
			public void go() {
				// GraphicsMain.MClass.Console = new ConsoleLogger(25, 50, 1200,
				// 300, GraphicsMain.MClass, GraphicsMain.MClass.comandmanager);
				// GraphicsMain.MClass.RealComponents.add(GraphicsMain.MClass.Console);
				read(input.getText());
				GraphicsMain.panel.remove(text);
				GraphicsMain.panel.remove(input);
				GraphicsMain.panel.remove(enter);
				GraphicsMain.panel.remove(progressValue);

			}
		});

	}

	@Override
	public void PrintComp(Graphics g) {
		g.setColor(new Color(181, 177, 177));
		g.fillRoundRect(this.x, this.y, this.sx, this.sy, 10, 10);
		this.input.PrintComp(g);
		this.enter.PrintComp(g);
		this.text.PrintComp(g);
		this.progressValue.PrintComp(g);
	}

	public void read(String fileName) {
		;
		// This will reference one line at a time
		String line = null;

		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			long totalLineCount = this.countRowsInFile(fileName);
			long currentLine = 0;
			ArrayList<Point> points = new ArrayList<Point>();
			ArrayList<specialPoint> specialpoints = new ArrayList<specialPoint>();

			while ((line = bufferedReader.readLine()) != null) {
				currentLine++;
				this.progress = totalLineCount / currentLine;
				this.progressValue.setTextSL("Progress: " + this.progress,
						new Color(52, 121, 217));

				if (line.endsWith("SpecialPoint")) {
					specialPoint sp = this.parseSpecialPoint(line);
					specialpoints.add(sp);
				} else {
					Point point = this.parseDataPoint(line);
					if (point != null) {
						points.add(point);
					}
				}
			}
			mainclass.RealComponents.remove(this);
			mainclass.addMapAndConsole();
			mainclass.map.addPoints(points);
			mainclass.map.robot.setSpecialPoints(specialpoints);
			try {
				ServerClass.StartServer();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			logger.FatalError("Unable to open file '" + fileName
					+ "' It cannot be found");
		} catch (IOException ex) {
			logger.FatalError("Error reading file '" + fileName + "'");
			// Or we could just do this:
			// ex.printStackTrace();
		}

	}
	/**
	 * Method used pre-05/12/15
	 * @param s
	 * @return
	 */
	public Point oldParsingMethod0511(String s){
		String[] data = s.split("/");
		int xloc = 1;
		int yloc = 2;
		int timesHitloc = 4;
		// logger.log("array size: "+data.length);
			Point p = new Point(Integer.parseInt(data[xloc]),
					Integer.parseInt(data[yloc]));
			p.setTimesHit(Integer.parseInt(data[timesHitloc]));
			return p;
	}
	public Point parseDataPoint(String s) {
		String[] data = s.split(",");
		int xloc = 1;
		int yloc = 2;
		int zloc = 3;
		int timesHitloc = 4;
		// logger.log("array size: "+data.length);
		if (Integer.parseInt(data[zloc]) == 0) {
			Point p = new Point(Integer.parseInt(data[xloc]),
					Integer.parseInt(data[yloc]));
			p.setTimesHit(Integer.parseInt(data[timesHitloc]));
			return p;
		}

		return null;
	}

	public specialPoint parseSpecialPoint(String s) {
		String[] data = s.split(",");
		int xloc = 1;
		int yloc = 2;
		return (new specialPoint(Integer.parseInt(data[xloc]),
				Integer.parseInt(data[yloc])));

	}

	public long countRowsInFile(String filename) {
		LineNumberReader lnr;
		try {
			lnr = new LineNumberReader(new FileReader(new File(filename)));
			lnr.skip(Long.MAX_VALUE);
			long lines = (lnr.getLineNumber() + 1);
			lnr.close();
			return lines;
		} catch (FileNotFoundException e1) {
			logger.FatalError("File not found.");
		} catch (IOException e) {
			e.printStackTrace();
			logger.FatalError("IO EXCP");
		}

		return 0;
	}

}
