package me.jy89hui.LoadMapStuff;

import java.awt.Color;
import java.awt.Graphics;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;

public class specialPoint extends SmartComponent {
	private ServerMain mainclass=ServerMain.main;
	private float x = 0;
	private float y = 0;
	public int size = 2;
	public Color color = new Color(255, 217, 0);
	public boolean hasNextPoint = false;
	public specialPoint nextPoint;

	public specialPoint(float y, float x) {
		this.x = y;
		this.y = x;
	}

	public specialPoint(float x, float y, specialPoint nextPT) {
		this.x = x;
		this.y = y;
		this.hasNextPoint = true;
		this.nextPoint = nextPT;
	}

	@Override
	public void PrintComp(Graphics g) {
		g.setColor(this.color);
		int[] locs = mainclass.map.pointToSwingLocation(this.x,
				this.y);
		int size = 1;
		g.fillRect(locs[0], locs[1], size, size);
		if (this.hasNextPoint) {
			g.setColor(this.color.darker());
			int[] nextLocs = mainclass.map.pointToSwingLocation(
					this.nextPoint.getLocX(), this.nextPoint.getLocY());
			// g.drawString(""+distanceTo(nextPoint.getLocX(),
			// nextPoint.getLocY()), locs[0]-5, locs[1]+30);
			// g.drawLine(locs[0], locs[1], nextLocs[0],nextLocs[1]);
		}

	}

	public float distanceTo(float x, float y) {
		double distX = Math.abs(this.x - x);
		double distY = Math.abs(this.y - y);
		return (float) Math.sqrt(Math.pow(distX, 2.0) + Math.pow(distY, 2));
	}

}
