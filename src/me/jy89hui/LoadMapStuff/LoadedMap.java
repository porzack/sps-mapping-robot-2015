package me.jy89hui.LoadMapStuff;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.ComponentScaler;
import me.jy89hui.Interfaces_2DRenderingSystem.DragBar;
import me.jy89hui.Interfaces_2DRenderingSystem.MovableComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class LoadedMap extends SmartComponent implements MovableComponent,
		MouseInputListener, KeyListener {
	private DragBar db;
	private ComponentScaler cs;
	private int mouseX = 0;
	private int mouseY = 0;
	private boolean Movable = true;
	private GraphicsMain gMain;
	public boolean careAboutAvgPoints = true;
	private ArrayList<LoadedPoint> points = new ArrayList<LoadedPoint>();
	public boolean showingPointData = false;
	public int centerX = 0;
	public int centerY = 0;
	private double radianToDegreeMultiplier = 360 / (2 * Math.PI);
	public ArrayList<specialPoint> specialPoints = new ArrayList<specialPoint>();
	// This is the constant that signifies how to average range when adding an
	// uncertain point
	private int cmOfRange = 5;

	public double sizeOf1Cm = 500.0; // meaning the translation in size of the
										// object, calculated in %

	public LoadedMap(int x, int y, int sx, int sy, boolean movable,
			GraphicsMain gm, ArrayList<LoadedPoint> points) {
		// GraphicsMain.MClass.map=this;
		JPanel jp = gm.getPanel();
		this.x = x;
		this.y = y;
		this.sy = sy;
		this.sx = sx;
		this.gMain = gm;
		this.Movable = movable;
		for (LoadedPoint p : points) {
			this.registerNewPoint(p);
		}
		this.setName("LoadedMap");
		if (this.Movable) {
			if (!(this instanceof MovableComponent)) {
				logger.FatalError("Cannot cast SmartComponent MAP to a MovableComponent");
			}
			this.db = new DragBar(this.x, this.y, this.sx, this.sy, this,
					this.gMain);
			this.cs = new ComponentScaler(this.x, this.y, this.sx, this.sy,
					this, this.gMain);
			this.cs.setMinX(600);
			jp.add(this.db);
			jp.add(this.cs);

		}
		jp.add(this);
		this.setFocusable(true);
		this.addKeyListener(this);
		addMouseListener(this);
		this.requestFocusInWindow();
		addMouseMotionListener(this);
		GraphicsMain.MClass.getPanel().addKeyListener(this);
		GraphicsMain.MClass.getFrame().addKeyListener(this);
		GraphicsMain.MClass.getFrame().add(this);

		this.Update(x, y, sx, sy);
	}

	public void registerNewPoint(LoadedPoint p) {
		this.points.add(p);
		int[] arry = this.pointToSwingLocation(p.getLocX(), p.getLocY());
		this.resizingNeeded(arry[0], arry[1]);
	}

	/**
	 * This method simply converts a location in the robots world into a world
	 * that swing can plot
	 * 
	 * @param p
	 * @return
	 */
	public int[] pointToSwingLocation(double x, double y) {
		x = x + this.centerX;
		y = y + this.centerY;
		double nx = (x / 100.0);
		double ny = (y / 100.0);
		nx = nx * this.sizeOf1Cm;
		ny = ny * this.sizeOf1Cm;
		int totalX = this.sx;
		int totalY = this.sy;
		nx = nx + (totalX / 2) + this.x;
		ny = ny + (totalY / 2) + this.y;

		int[] toReturn = { (int) nx, (int) ny };
		return toReturn;

	}

	public int[] swingToPointLocation(double x, double y) {
		x = x - this.centerX;
		y = y - this.centerY;
		double nx = (x * this.sizeOf1Cm);
		double ny = (y * this.sizeOf1Cm);
		int totalX = this.sx;
		int totalY = this.sy;
		nx = nx - (totalX / 2) - this.x;
		ny = ny - (totalY / 2) - this.y;
		int[] toReturn = { (int) nx, (int) ny };
		return toReturn;
		/*
		 * int[] center = {this.centerX, this.centerY}; int diffX = (int)
		 * (center[0]-x); int diffY = (int) (center[1]-y); int totalDist= (int)
		 * Math.sqrt(diffX^2+diffY^2); double angle =
		 * Math.toDegrees(Math.atan(diffY/diffX)); float x1 =(float) (this.x +
		 * (Math.cos(angle) * totalDist)); float y1 = (float) (this.y +
		 * (Math.sin(angle) * totalDist)); double nx = (x1 / 100.0); double ny =
		 * (y1 / 100.0); nx = nx * this.sizeOf1Cm; ny = ny * this.sizeOf1Cm;
		 * int[] toReturn = { (int) ny, (int) ny }; return toReturn;
		 */

	}

	/**
	 * This method converts the size of an object into the correct sizing
	 * 
	 * @param x
	 * @return
	 */
	public int applySizeOf1Cm(int x) {
		// add 0.6 to force round up the int
		return (int) Math.ceil(((x / 100.0) * this.sizeOf1Cm));
	}

	public float applySizeOf1Cm(float x) {
		return (float) ((x / 100.0) * this.sizeOf1Cm);
	}

	public double applySizeOf1Cm(double x) {
		return ((x / 100.0) * this.sizeOf1Cm);
	}

	// XY meaning the objects current location, we will not resize until some
	// object is outside the frame
	public void resizingNeeded(float x1, float y1) {
		int outsideArea = 10;
		int x = this.pointToSwingLocation(x1, y1)[0];
		int y = this.pointToSwingLocation(x1, y1)[1];
		boolean invalid = false;
		if ((this.x + this.sx) < x + outsideArea) {
			invalid = true;
		} else if ((this.y + this.sy) < y + outsideArea) {
			invalid = true;
		} else if (this.x > x - outsideArea) {
			invalid = true;
		} else if (this.y > y - outsideArea) {
			invalid = true;
		} else {
			return;
		}
		while (invalid) {
			this.sizeOf1Cm = this.sizeOf1Cm - 0.5; // 0.5 is how much we go down
													// each time
			x = this.pointToSwingLocation(x1, y1)[0];
			y = this.pointToSwingLocation(x1, y1)[1];
			invalid = false;
			if ((this.x + this.sx) < x) {
				invalid = true;
			} else if ((this.y + this.sy) < y) {
				invalid = true;
			} else if (this.x > x) {
				invalid = true;
			} else if (this.y > y) {
				invalid = true;
			} else {
				return;
			}
		}

	}

	@Override
	public void onCommand(Command c) {
		if (c.getArgs()[1].equalsIgnoreCase("SetSize")) {
			try {
				this.sx = Integer.parseInt(c.getArgs()[2]);
				this.sy = Integer.parseInt(c.getArgs()[3]);
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName()
						+ " Setsize <SizeXInt> <SizeYInt>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("SetLocation")) {
			try {
				this.TranslateLocation(
						this.x - Integer.parseInt(c.getArgs()[2]), this.y
								- Integer.parseInt(c.getArgs()[3]));
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName()
						+ " SetLocation <LocXInt> <LocYInt>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("setname")) {
			try {
				this.setName(c.getArgs()[2]);
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName() + " Setname <NameString>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("SomeMenuCommand")) {
			try {
				logger.log("PROGRESS");
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName() + " Something... ");
			}
		} else {

		}
	}

	public ArrayList<LoadedPoint> getPoints() {
		return this.points;
	}

	public void setPoints(ArrayList<LoadedPoint> points) {
		this.points = points;
	}

	@Override
	public void PrintComp(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(new Color(0, 0, 0));
		g2d.fillRoundRect(this.x, this.y, this.sx, this.sy, 10, 10);
		for (int x = 0; x < this.getPoints().size(); x++) {
			this.getPoints().get(x).PrintComp(g);
		}
		if (this.Movable) {
			this.db.PrintComp(g);
			this.cs.PrintComp(g);
		}
		for (specialPoint p : this.specialPoints) {
			p.PrintComp(g);
		}
		// Do the hover stuff with points
		this.pointDetails(g);

	}

	public void pointDetails(Graphics g) {
		if (this.showingPointData) {
			for (LoadedPoint p : this.points) {
				int xx = this.pointToSwingLocation(p.getLocX(), p.getLocY())[0];
				int yy = this.pointToSwingLocation(p.getLocX(), p.getLocY())[1];
				int disX = Math.abs(this.mouseX - xx);
				int disY = Math.abs(this.mouseY - yy);
				if (disX < 10 && disY < 10) {
					int x = this.mouseX + 20;
					int y = this.mouseY + 20;
					g.setColor(new Color(10, 10, 10));
					g.fillRoundRect(x, y, 150, 100, 10, 10);
					g.setColor(new Color(90, 90, 255));
					g.drawRoundRect(x, y, 150, 100, 10, 10);
					g.setColor(new Color(100, 150, 250));
					g.drawString("x:" + p.getLocX(), x + 10, y + 15);
					g.drawString("y:" + p.getLocY(), x + 70, y + 15);
					g.drawString("Quality:" + (int) (p.getQuality()), x + 10,
							y + 30);
					g.drawString("TimesHit:" + p.getTimesHit(), x + 10, y + 45);
					Color color = p.getColorBasedOnQuality();
					g.drawString(
							"Color: [" + color.getRed() + ","
									+ color.getGreen() + "," + color.getBlue()
									+ "]", x + 10, y + 90);
				}
			}
		}
	}

	@Override
	public void TranslateLocation(int dx, int dy) {
		this.x = this.x - dx;
		this.y = this.y - dy;
		this.Update(this.x, this.y, this.sx, this.sy);
	}

	@Override
	public void TranslateSize(int sx, int sy) {
		this.sx = this.sx - sx;
		this.sy = this.sy - sy;
		this.Update(this.x, this.y, this.sx, this.sy);
	}

	public void Update(int x, int y, int sx, int sy) {
		this.x = x;
		this.y = y;
		this.sy = sy;
		this.sx = sx;
		this.setBounds(this.x, this.y, this.sx, this.sy);

		if (this.Movable) {
			this.db.Update(this.x, this.y, this.sx + 150, this.sy);
			this.cs.Update(this.x, this.y, this.sx + 150, this.sy);
		}
	}

	public int numberOfPoints() {
		return this.getPoints().size();
	}

	public double getAvgQuality() {
		long total = 0;
		for (LoadedPoint p : this.getPoints()) {
			total = (long) (total + p.getQuality());
		}
		try {
			return (total / this.getPoints().size());
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		int[] loc = this.swingToPointLocation(this.mouseX, this.mouseY);
		if (this.specialPoints.size() > 0) {
			this.specialPoints.add(new specialPoint(loc[0], loc[1],
					this.specialPoints.get(this.specialPoints.size() - 1)));
		} else {
			this.specialPoints.add(new specialPoint(loc[0], loc[1]));
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		this.mouseX = arg0.getX();
		this.mouseY = arg0.getY();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			logger.log("Enter");
		} else if (e.getKeyCode() == KeyEvent.VK_EQUALS) {

			this.sizeOf1Cm = this.sizeOf1Cm + 10;
		} else if (e.getKeyCode() == KeyEvent.VK_MINUS) {
			for (int x = 0; x < 10; x++) {
				if (this.sizeOf1Cm > 2) {
					this.sizeOf1Cm = this.sizeOf1Cm - 1;
				}
			}

		} else if (e.getKeyCode() == KeyEvent.VK_UP) {
			this.centerY = this.centerY + 50;
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			this.centerY = this.centerY - 50;
		} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			this.centerX = this.centerX + 50;
		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			this.centerX = this.centerX - 50;
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
