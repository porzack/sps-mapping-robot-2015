package me.jy89hui.MappingStuff;

import java.awt.Color;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Program;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class RobotTestClass implements Program{

	@Override
	public void Load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Run() {
		logger.log("--------------------------------------------------", new Color(0,20,255));
		logger.log("Console now controlled by the Robot testing class", new Color(0,0,255));
		GraphicsMain.Console.RequestNextCommands(this, Integer.MAX_VALUE);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Tester";
	}

	@Override
	public void giveData(String text) {
		//GraphicsMain.MClass.map.robot.parseResult(text);
	}

}
