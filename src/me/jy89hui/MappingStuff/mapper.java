package me.jy89hui.MappingStuff;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Action;
import me.jy89hui.LoadMapStuff.specialPoint;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class mapper implements Runnable {
	private static ServerMain mainclass = ServerMain.main;
	private static Sector sector;
	public static boolean movedLastTime = false;
	public static Sector lastSector = mainclass.map.sectors.get(0);
	private Robot robot= null;
	public static HashMap<Integer, int[]> conversionMap= new HashMap<Integer,int[]>();
	// ArrayList<String> toSend= new ArrayList<String>();
	public static void testMapping() throws InterruptedException {
		logger.log("Mapping Started!", new Color(50, 50, 200));
		Robot robot = mainclass.map.robot;
		robot.turnToAbsolute(0);
		robot.scannerTurnToAbsolute(0);
		robot.scanFromTo(-160, 160, 2);

	}
	public void loadConversionMap(){
			String line = null;
			String fileName = "ConversionMap.csv";
			try {
				FileReader fileReader = new FileReader(fileName);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				ArrayList<Point> points = new ArrayList<Point>();
				ArrayList<specialPoint> specialpoints = new ArrayList<specialPoint>();
				int counter=0;
				while ((line = bufferedReader.readLine()) != null) {
					String[] data = line.split("/");
					int readingValue = Integer.parseInt(data[0]);
					int realValue = Integer.parseInt(data[1]);
					int[] combined = {readingValue, realValue};
					this.conversionMap.put(counter, combined);
					counter++;
				}

				bufferedReader.close();
			} catch (FileNotFoundException ex) {
				logger.FatalError("Unable to open file '" + fileName
						+ "' It cannot be found");
			} catch (IOException ex) {
				logger.FatalError("Error reading file '" + fileName + "'");
			}
	}
	public void writeConversionMap(){
		File file = new File("ConversionMap" + ".csv");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileWriter fw;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			int x = 0;
			for (x = 0; x < this.conversionMap.size(); x++) {
				int[] value = this.conversionMap.get(x);
				fw.write(value[0]+"/"+value[1]);
			}

			bw.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	

	public static Sector determineNextSector() {
		logger.log("Determining next sector..");
		Robot robot = mainclass.map.robot;
		// ArrayList<Point> points = mainclass.map.getPoints();
		// int[] angles = new int[360]; // for every degree
		float[] qualities = new float[8];
		Sector[] sectors = new Sector[8];
		for (int x = 0; x < mainclass.map.sectors.size(); x++) {
			qualities[x] = mainclass.map.sectors.get(x)
					.getAvgQuality();
			sectors[x] = mainclass.map.sectors.get(x);
		}
		String movingTo = "leastQuality";

		if (movingTo.equalsIgnoreCase("mostQuality")) {
			// Not implemented yet
		} else if (movingTo.equalsIgnoreCase("leastQuality")) {
			for (Sector s : sectors) {
				logger.log("Sector [" + s.getStart() + "," + s.getEnd()
						+ "] has a total of " + s.getPoints().size()
						+ " and avg quality of: " + s.getAvgQuality(),
						new Color(150, 90, 90));
			}
			Sector leastSector = sectors[findLeast(qualities)];
			logger.log(
					"Sector [" + leastSector.getStart() + ","
							+ leastSector.getEnd()
							+ "]  has been chosen (quality "
							+ leastSector.getAvgQuality() + ")", new Color(90,
							150, 90));
			return leastSector;
		} else if (movingTo.equalsIgnoreCase("leastPoints")) {

		}
		return null;

	}

	public static int findLeast(float[] numbers) {
		float lastnumber = numbers[0];
		int best = 0;
		for (int x = 0; x < numbers.length; x++) {
			if (numbers[x] < lastnumber) {
				lastnumber = numbers[x];
				best = x;
			}
		}
		ArrayList<Integer> numbersWithLowest = new ArrayList<Integer>();
		for (int x = 0; x < numbers.length; x++) {
			if (numbers[x] == lastnumber) {
				numbersWithLowest.add(x);
			}
		}
		Random rand = new Random();
		if (numbersWithLowest.size() > 1) {
			return numbersWithLowest
					.get(rand.nextInt(numbersWithLowest.size()));
		} else {
			return best;
		}
	}

	public static int findLeast(ArrayList<Float> numbers) {
		float lastnumber = numbers.get(0);
		int best = 0;
		for (int x = 0; x < numbers.size(); x++) {
			if (numbers.get(x) < lastnumber) {
				lastnumber = numbers.get(x);
				best = x;
			}
		}
		return best;
	}

	public static void setSector(Sector s) {
		sector = s;
	}

	public static Sector getSector() {
		return sector;
	}

	/**
	 * Algorithm1: { Look widely left and right (e.g. -160 to 160) find sector
	 * w/lowest avg quality points (if multiple sectors have same low quality,
	 * use first) scan sector move 1/3 of the way towards the nearest point }
	 * repeat
	 * 
	 * @throws InterruptedException
	 */
	public void alg1Run() throws InterruptedException {
		logger.log("started");
		ActionBuffer ab = new ActionBuffer();
		// final Robot robot = mainclass.map.robot;
		ab.addActionToList(new Action() {
			@Override
			public void go() {
				logger.log("Do a full scan: scanning from to: -160, 160");
				mainclass.map.robot.scanFromTo(-160, 160, 2);
			}
		});
		// Find the lowest quality sector and turn the robot to it's center
		ab.addActionToList(new Action() {
			@Override
			public void go() {
				mapper.setSector(determineNextSector());
				Robot rbot = mainclass.map.robot;
				Sector sector = mapper.getSector();
				if (sector == mapper.lastSector && !mapper.movedLastTime) {
					Random rand = new Random();
					mapper.setSector(mainclass.map.sectors.get(rand
							.nextInt(mainclass.map.sectors.size())));
					sector = mapper.getSector();
				}
				logger.log("Turning to center of my chosen sector: "
						+ sector.getCenterAngle());
				mainclass.map.robot.turnToAbsolute(sector
						.getCenterAngle());

			}
		});
		ab.addActionToList(new Action() {
			@Override
			public void go() {
				Sector sector = mapper.getSector();
				Robot rbot = mainclass.map.robot;
				mainclass.map.robot.scanFromTo((float) -22.5,
						(float) 22.5, 1);
			}
		});

		ab.addActionToList(new Action() {
			@Override
			public void go() {
				Sector sector = mapper.getSector();
				Point point = sector.findNearestPoint();
				Robot rbot = mainclass.map.robot;
				double distToRobot = point.getDistanceToRobotHypotenuse();
				logger.log("Nearest Point: [" + point.getLocX() + ","
						+ point.getLocY() + "] Robot at: [ "
						+ rbot.getLocationX() + "," + rbot.getLocationY() + "]");
				logger.log("Absolute angle from robot to point:"
						+ point.getAngleFromRobot());
				logger.log("dist: [x,y] = [" + point.getDistanceToRobot()[0]
						+ "," + point.getDistanceToRobot()[1]
						+ "] calculated distance: " + distToRobot);
				float dist = (float) (point.getDistanceToRobotHypotenuse() / 3);
				// Only move if we're farther away than 60 cm. Otherwise rescan
				if (dist > 15) {// This way it will only move to a closeness of
								// 60.
					mainclass.map.robot.move(dist);
					logger.log("moving " + dist);
					mapper.movedLastTime = true;
				} else {
					logger.normalError("DISTANCE LESS THAN 15");
					rbot.setScannerLaserPointerStatus(0);
					mapper.movedLastTime = false;
				}
			}
		});
		logger.log("added all ");
		mainclass.map.robot.setActionBuffer(ab);
		boolean doneWithCmds = ab.hasCmdsLeft();
		while (doneWithCmds) {
			doneWithCmds = ab.hasCmdsLeft();
			// logger.log("has "+ab.getCmdsLeft()+" cmds left");
			Thread.sleep(100);
		}
		mainclass.map.robot.addSpecialPoint();

		
	}
	public void distanceTesterRun() throws InterruptedException {
		logger.log("started");
		ActionBuffer ab = new ActionBuffer();
		// final Robot robot = mainclass.map.robot;
		for (int x=0; x<300; x++){
			ab.addActionToList(new Action() {
				@Override
				public void go() {
					logger.log("Do a full scan: scanning from to: -160, 160");
					mainclass.map.robot.move(5);
					mainclass.map.robot.scanWithQuality(100);
					//mainclass.map.robot.
				}
			});
		}
		logger.log("added all ");
		mainclass.map.robot.setActionBuffer(ab);
		boolean doneWithCmds = ab.hasCmdsLeft();
		while (doneWithCmds) {
			doneWithCmds = ab.hasCmdsLeft();
			// logger.log("has "+ab.getCmdsLeft()+" cmds left");
			Thread.sleep(100);
		}
		//mainclass.map.robot.addSpecialPoint();

	}
	public void alg2Run() throws InterruptedException {
		logger.log("started");
		ActionBuffer ab = new ActionBuffer();
		final Robot robot = mainclass.map.robot;
		ab.addActionToList(new Action() {
			@Override
			public void go() {
				robot.scanFromTo(-170, 170, 2);
			}
		});
		ab.addActionToList(new Action() {
			@Override
			public void go() {
				robot.turnRelative(90);
			}
		});

		logger.log("added all ");
		mainclass.map.robot.setActionBuffer(ab);
		logger.log("told to do next");
		logger.log("did next");
		boolean doneWithCmds = ab.hasCmdsLeft();
		while (doneWithCmds) {
			doneWithCmds = ab.hasCmdsLeft();
			// logger.log("has "+ab.getCmdsLeft()+" cmds left");
			Thread.sleep(100);

		}

	}

	@Override
	public void run() {
		while (true) {
			try {
				this.alg1Run();
				Thread.sleep(200);
				mainclass.map.robot.setScannerLaserPointerStatus(0);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

		}

	}
}