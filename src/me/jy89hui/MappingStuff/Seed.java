package me.jy89hui.MappingStuff;

import java.awt.Color;
import java.awt.Graphics;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Location;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;

public class Seed extends SmartComponent{
	private ServerMain mainclass = ServerMain.main;
	public boolean active=true;
	private boolean paintLineToFather=true;
	private boolean paintIfNonActive=true;
	private Location loc = new Location(0,0);
	private int size=5;
	private Seed[] children=null;
	private Seed father=null;
	public Seed (Location location, Seed father){
		this.loc=location;
		this.father=father;
		
	}
	public Seed (Location location){
		this.loc=location;
	}
	public void setChildren(Seed[] children){
		this.children=children;
	}
	public Location getLoc(){
		return this.loc;
	}
	public Seed[] getChildren(){
		return this.children;
	}
	@Override
	public void PrintComp(Graphics g) {
		if (!this.active && !paintIfNonActive) return;
		int[] loc = mainclass.map.pointToSwingLocation(this.loc.x, this.loc.y);
		if (this.father != null && paintLineToFather){
			g.setColor(new Color(204, 170,18));
			int[] fatherLoc = mainclass.map.pointToSwingLocation(father.getLoc().x, father.getLoc().y);
			g.drawLine(loc[0],loc[1], fatherLoc[0], fatherLoc[1]);
		}
		g.setColor(new Color(163,134,2));
		g.fillRect(loc[0]-(size/2), loc[1]-(size/2), size, size);
	}

}
