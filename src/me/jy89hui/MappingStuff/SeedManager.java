package me.jy89hui.MappingStuff;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Location;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class SeedManager implements Runnable{
	private Thread thisThread;
	private ServerMain mainclass = ServerMain.main;
	private boolean terminated = false;
	private long maxSeeds = 10000;
	private ArrayList<Point> points;
	private ArrayList<Seed> seeds = new ArrayList<Seed>();
	private float[] angles;
	private Location startingLoc=null;
	// Number of seeds created per split
	private int splitQuantity;
	// Mininmum range that each seed must be from the nearest point
	private int rangeFromNearest=12;
	// min distance that seeds have to be seperated by
	private int minSeedDistance=10;
	// For rangeFromNearest
	private int minimumTimesHitNeededToCountAsAPoint=3;
	private int distance=15;
	private double radianToDegreeMultiplier = 360.0 / (2 * Math.PI);
	public SeedManager(Location loc,ArrayList<Point> points, float[] angles ){
		this.startingLoc=loc;
		create(points, angles);
	}
	public SeedManager(Location loc,ArrayList<Point> points, float splitQuantity){
		this.startingLoc=loc;
		float inc = (float) (360.0/splitQuantity);
		float[] angles = new float[(int) splitQuantity];
		for (int x=0; x<splitQuantity; x++){
			angles[x]=inc*x;
		}
		create(points, angles);
	}
	private void create(ArrayList<Point> points, float[] angles){
		this.points=points;
		this.angles=angles;
		this.splitQuantity=angles.length;
		this.seeds.add(new Seed(this.startingLoc));
		thisThread= new Thread(this);
		thisThread.start();
	}
	
	private void setRangeFromNearest(int cm){
		this.rangeFromNearest=cm;
	}
	/**
	 *  Basically germinate, have all seeds create new seeds at certain angles and locations.
	 * @return number of points added
	 */
	private int expand(){
		if (terminated) return 0;
		logger.log("Size: "+this.seeds.size());
		ArrayList<Seed> seedsToAdd=new ArrayList<Seed>();
		for (Seed s : this.seeds){
			if (s.active){
				//logger.log("Chosen seed at: "+s.getLoc().toString());
				for (float f : this.angles){
					//logger.log("  Trying angle: "+f);
					// add new point in each direction	
					Location loc = new Location((s.getLoc().x + (Math.cos(f
							/ this.radianToDegreeMultiplier) * this.distance)), 
					(s.getLoc().y + (Math.sin(f
							/ this.radianToDegreeMultiplier) * this.distance)));
					//logger.log("  Selected "+loc.toString());
					boolean newLocation = true;
					for (Seed sed : this.seeds){
						int distX = (int) Math.abs(loc.x-sed.getLoc().x);
						int distY = (int) Math.abs(loc.y-sed.getLoc().y);
						if (distX<=this.minSeedDistance&&distY<=this.minSeedDistance){
							newLocation=false;
						}
					}
					for (Seed sed : seedsToAdd){
						int distX = (int) Math.abs(loc.x-sed.getLoc().x);
						int distY = (int) Math.abs(loc.y-sed.getLoc().y);
						if (distX<=this.minSeedDistance&&distY<=this.minSeedDistance){
							newLocation=false;
						}
					}
				//	logger.log("  Passed seed test. Starting point test");
					if (newLocation){
						for (Point p : this.points){
							if (p.getTimesHit()>=this.minimumTimesHitNeededToCountAsAPoint){
								// Note: Using sqrt(a^2+b^2) would be better although is too computer intensive to do thousands of times.
								int distX = (int) Math.abs(loc.x-p.getLocX());
								int distY = (int) Math.abs(loc.y-p.getLocY());
								if (distX<=this.rangeFromNearest&&distY<=this.rangeFromNearest){
									newLocation=false;
									//logger.log("   A point is too close. PointLoc: ["+p.getLocX()+","+p.getLocY()+"], dist is: "+dist+ " and point quality of "+p.getTimesHit() );
								}
							}
						}
					}
					if (newLocation){
						seedsToAdd.add(new Seed(loc,s));
					}
				}
				s.active=false;
			}
		}
		for (Seed sed : seedsToAdd){
			this.seeds.add(sed);
		}
		if (this.seeds.size()>=this.maxSeeds){
			this.terminate();
		}
		return seedsToAdd.size();

	}
	public void terminate(){
		this.terminated=true;
		logger.log("Seed Manager ");
		logger.logLastLine("Terminated.", new Color(200,50,50));
		logger.logLastLine(" Seed count: "+this.seeds.size(), new Color(0,0,0));
		thisThread.stop();
	}
	public void paintSeeds(Graphics g){
		try{
		for (Seed s: this.seeds){
			s.PrintComp(g);
		}
		} catch(ConcurrentModificationException e){
			this.paintSeeds(g);
		}
	}
	@Override
	public void run() {
		while (true){
			mainclass.beginTimeMeasurement(15);
			if (this.expand()==0){
				this.terminate();
			}
			mainclass.endTimeMeasurement(15, true);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
