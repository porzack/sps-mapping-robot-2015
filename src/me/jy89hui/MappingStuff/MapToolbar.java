package me.jy89hui.MappingStuff;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.swing.JPanel;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Action;
import me.jy89hui.Interfaces_2DRenderingSystem.Location;
import me.jy89hui.Interfaces_2DRenderingSystem.MovableComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartButton;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartButton.typeOfButton;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartLog;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartTextArea;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartTextArea.typeOfTextArea;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class MapToolbar extends SmartComponent implements MovableComponent {
	/*
	 * We have SX set as a constant so that it is not readjusted with the rest
	 * of the map Cannot be changed
	 */
	private SeedManager sm=null;
	private ServerMain mainclass = ServerMain.main;
	private Date startTime;
	private SmartTextArea rezOfACm;
	private SmartButton forceScan;
	private SmartButton showRegions;
	private SmartButton apply;
	private SmartButton PointData;
	private SmartButton saveButton;
	private SmartButton startMapping;
	private SmartButton testSeed;
	private SmartButton Test2;
	private SmartTextArea mapName;

	public MapToolbar(int x, int y, int sy, GraphicsMain gm) {
		this.startTime = new Date(); // Define when we start the program, not
										// before
		JPanel jp = gm.getPanel();
		this.x = x;
		this.y = y;
		this.sx = 150;
		this.sy = sy;
		this.mapName = new SmartTextArea(this.x + this.sx + 20, y + 20, 110, 30, mainclass);
		this.mapName.textAreaType = typeOfTextArea.TextInput;
		this.mapName.setText("[NameOfMap]");
		this.saveButton = new SmartButton(this.x + this.sx + 20, this.y
				+ this.sy - 400, 100, 40, "Save", mainclass);
		this.saveButton.setText(new SmartLog("Save Map", new Font("Arial",
				Font.PLAIN, 15)));
		this.testSeed = new SmartButton(this.x + this.sx + 20, this.y
				+ this.sy - 550, 100, 40, "Save", mainclass);
		this.testSeed.setText(new SmartLog("Test Seeding.", new Font(
				"Courier", Font.PLAIN, 10)));
		this.testSeed.enableAction(new Action() {
			@Override
			public void go() {
				Robot r = mainclass.map.robot;
				//float[] angles = {0,90,180,270};
				//float[] angles = {0,45,90,135,180,225,270,315};
				//float[] angles = {0,10,2};
				sm = new SeedManager(new Location(r.getLocationX(), r.getLocationY()), mainclass.map.getPoints(), 2.6f);

			}
		});
		this.startMapping = new SmartButton(this.x + this.sx + 20, this.y
				+ this.sy - 700, 100, 40, "Save", mainclass);
		this.startMapping.setText(new SmartLog("Begin Map", new Color(50, 140,
				140), new Font("Arial", Font.PLAIN, 15)));
		this.startMapping.enableAction(new Action() {
			@Override
			public void go() {
				(new Thread(new mapper())).start();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				mainclass.map.robot.getActionBuffer()
						.fuckingDoTheNextAction();
			}
		});
		this.saveButton.enableAction(new Action() {
			@Override
			public void go() {
				logger.log("Saving map...", new Color(50, 50, 150));
				// /users/Zack/Desktop/Yes.txt
				File file = new File(getNameOfMap() + ".csv");

				// if file doesnt exists, then create it
				if (!file.exists()) {
					try {
						file.createNewFile();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				FileWriter fw;
				try {
					fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					int x = 0;
					for (x = 0; x < getPoints().size(); x++) {
						Point p = getPoints().get(x);
						if (p.getQuality() > 0) {
							if (p.getTimesHit() > 1) {
								for (int z = 0; z <= 60; z+=3) {
									int elevation = (int) ((int) (Math.random()*3+z)*Math.signum(z));
									bw.write(x + "," + p.getLocX() + ","
											+ p.getLocY() + "," + elevation + ","
											+ p.getTimesHit() + "\n");
								}
							} else {
								bw.write(x + "," + p.getLocX() + ","
										+ p.getLocY() + "," + 0 + ","
										+ p.getTimesHit() + "\n");
							}
						}
					}

					bw.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				logger.log("Saved!", new Color(50, 50, 190));
			}
		});
		jp.add(this.startMapping);
		this.PointData = new SmartButton(this.x + this.sx + 20, this.y
				+ this.sy - 200, 100, 40, "PointData", mainclass);
		this.PointData.setButtonType(typeOfButton.Options);
		ArrayList<SmartLog> q = new ArrayList<SmartLog>();
		q.add(new SmartLog("PointData: Shown", new Color(50, 250, 50)));
		q.add(new SmartLog("PointData: Hidden", new Color(250, 50, 50)));

		this.PointData.setOptions(q);
		jp.add(this.PointData);
		this.Test2 = new SmartButton(this.x + this.sx + 20, this.y + this.sy
				- 150, 100, 40, "test2", mainclass);
		this.Test2.setButtonType(typeOfButton.TrueFalse);
		jp.add(this.Test2);
		jp.add(this.testSeed);
		this.rezOfACm = new SmartTextArea(this.x + 20, this.y + 20, 20, 10, mainclass);
		this.rezOfACm.textAreaType = typeOfTextArea.TextInput;
		jp.add(this.rezOfACm);
		this.apply = new SmartButton(this.x + this.sx + 20, this.y + this.sy
				- 30, 100, 40, new SmartLog("Apply", new Color(50, 50, 255)), mainclass);
		jp.add(this.apply);
		this.apply.enableAction(new Action() {
			@Override
			public void go() {
				logger.log("Actions applied.");
			}
		});
		this.Update(x, y, sy); // / Update must me last so others can be defined
	}

	public ArrayList<Point> getPoints() {
		// TODO Auto-generated method stub
		return mainclass.map.getPoints();
	}

	public String getNameOfMap() {
		return this.mapName.getText();
	}

	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	@Override
	public void PrintComp(Graphics g) {
		g.setColor(new Color(150, 150, 150));
		g.fillRoundRect(this.x, this.y, this.sx, this.sy, 10, 10);
		g.setColor(new Color(100, 100, 100));
		g.drawRoundRect(this.x, this.y, this.sx, this.sy, 10, 10);
		Font f = g.getFont();
		g.setFont(new Font("Serif", Font.BOLD, 12));

		int textY = 75;
		g.drawString("Port:" + ServerClass.getPort(), this.x + 10, this.y
				+ textY);
		textY = textY + 20;
		g.drawString("Robot:", this.x + 10, this.y + textY);
		textY = textY + 13;
		g.drawString("  LocX: " + mainclass.map.robot.getLocationX(),
				this.x + 10, this.y + textY);
		textY = textY + 10;
		g.drawString("  LocY: " + mainclass.map.robot.getLocationY(),
				this.x + 10, this.y + textY);
		textY = textY + 10;
		g.drawString(
				"  Total Time: "
						+ this.getDateDiff(this.startTime, new Date(),
								TimeUnit.MINUTES) + " M", this.x + 10, this.y
						+ textY);
		textY = textY + 12;
		g.drawString("  BaseRotation: "
				+ mainclass.map.robot.currentBaseRotation,
				this.x + 10, this.y + textY);
		textY = textY + 10;
		g.drawString("  LaserRotation: "
				+ mainclass.map.robot.currentLaserRotation,
				this.x + 10, this.y + textY);
		textY = textY + 10;
		g.drawString("  TotalRotation: "
				+ mainclass.map.robot.currentTotalRotation,
				this.x + 10, this.y + textY);
		textY = textY + 10;
		g.drawString("  CmdsInBuffer: "
				+ mainclass.map.robot.cmdsLeft, this.x + 10, this.y
				+ textY);
		textY = textY + 10;
		g.drawString("  Time per Reading: " + "TODO", this.x + 10, this.y
				+ textY);
		textY = textY + 10;
		g.drawString("  Battery%: " + "TODO", this.x + 10, this.y + textY);
		textY = textY + 20;
		g.drawString("Points: " + mainclass.map.numberOfPoints(),
				this.x + 10, this.y + textY);
		textY = textY + 12;
		g.drawString(
				"AvgQuality: " + (int) mainclass.map.getAvgQuality(),
				this.x + 10, this.y + textY);
		textY = textY + 12;
		g.drawString("Steps per cm: " + "TODO", this.x + 10, this.y + textY);
		textY = 900;
		g.drawString("Size of a cm: " + (int) mainclass.map.sizeOf1Cm
				+ "%", this.x + 10, this.y + textY);
		textY = textY + 12;
		g.drawString("Quality: ", this.x + 10, this.y + textY);
		textY = textY + 12;
		g.drawString(": ", this.x + 10, this.y + textY);
		this.mapName.PrintComp(g);
		this.apply.PrintComp(g);
		this.PointData.PrintComp(g);
		this.Test2.PrintComp(g);
		this.saveButton.PrintComp(g);
		this.startMapping.PrintComp(g);
		this.testSeed.PrintComp(g);
		if(this.sm != null){
			this.sm.paintSeeds(g);
		}
		boolean toShowPointData = false;
		if (this.PointData.getSelectedOption().getMessage()
				.equalsIgnoreCase("PointData: Shown")) {
			toShowPointData = true;
		} else if (this.PointData.getSelectedOption().getMessage()
				.equalsIgnoreCase("PointData: Hidden")) {
			toShowPointData = false;
		}
		mainclass.map.showingPointData = toShowPointData;
		g.setFont(f); // Reset font
	}

	public void Update(int x, int y, int sy) {
		this.x = x;
		this.y = y;
		this.sy = sy;
		this.setBounds(this.x, this.y, this.sx, this.sy);
		this.rezOfACm.Update(this.x + 80, this.y + 7, 25, 20);
		this.apply.Update(this.x + this.sx - 125, this.y + this.sy - 80, 100,
				40);
	}

	@Override
	public void TranslateLocation(int dx, int dy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void TranslateSize(int sx, int sy) {
		// TODO Auto-generated method stub

	}

}
