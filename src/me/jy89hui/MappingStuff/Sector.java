package me.jy89hui.MappingStuff;

import java.util.ArrayList;

import me.jy89hui.AMain.ServerMain;

public class Sector {
	private ServerMain mainclass = ServerMain.main;
	float start = 0;
	float end = 0;
	float range = Math.abs(end - start);
	ArrayList<Point> points = new ArrayList<Point>();

	public Sector(float starting, float ending) {
		this.start = starting;
		this.end = ending;
		this.range = Math.abs(end - start);
	}

	public float getStart() {
		return this.start;
	}

	public float getEnd() {
		return this.end;
	}

	public float getStartRelativeToRobot() {
		float start = this.start
				- mainclass.map.robot.currentBaseRotation;
		return start;
	}

	public float getEndRelativeToRobot() {
		float end = this.end
				- mainclass.map.robot.currentBaseRotation;
		return end;
	}

	public float getCenterAngle() {
		this.range = Math.abs(end - start);
		return (this.getStart() + (this.range / 2));
	}

	public void addPoint(Point p) {
		float degree = p.getAngleFromRobot();
		// logger.silentlog("Trying to add point. Degree:"+degree+" starting: "+this.getStart()+" end:"+this.getEnd());
		if (degree >= this.getStart() && degree < this.getEnd()) {
			this.points.add(p);
			// logger.silentlog("added point");
		}
	}

	public void setPoints(ArrayList<Point> pts) {
		// logger.silentlog("setpoints called. "+pts.size()+" points");
		this.points = new ArrayList<Point>();
		for (Point p : pts) {
			this.addPoint(p);
		}
		// logger.silentlog("after add: "+this.points.size()+" points added");
	}

	public Point findNearestPoint() {
		Point closest = this.points.get(0);
		for (Point p : this.points) {
			if (p.getDistanceToRobotHypotenuse() < closest
					.getDistanceToRobotHypotenuse() && p.getQuality() > 0) {
				closest = p;
			}
		}
		return closest;
	}

	public Point findNearestPointAtAngle(int angle) {
		ArrayList<Point> sameAngle = new ArrayList<Point>();
		for (Point p : this.points) {
			if ((int) p.getAngleFromRobot() == angle) {
				sameAngle.add(p);
			}
		}
		if (sameAngle.size() > 0) {
			double bestDist = sameAngle.get(0).getDistanceToRobotHypotenuse();
			Point bestPoint = sameAngle.get(0);
			for (Point p : sameAngle) {
				if (p.getDistanceToRobotHypotenuse() < bestDist) {
					bestDist = p.getDistanceToRobotHypotenuse();
					bestPoint = p;
				}

			}
			return bestPoint;
		}
		return null;

	}

	public float getAvgQuality() {
		// logger.log("getting avg quality");
		double quality = 0;
		int todev = 0;
		ArrayList<Point> nearPoints = new ArrayList<Point>();
		for (int x = 0; x < 45; x++) {
			nearPoints
					.add(this.findNearestPointAtAngle((int) (x + this.start)));
		}
		// logger.log("found "+nearPoints.size()+" pts");
		for (Point p : nearPoints) {
			if (p != null) {
				if (p.getQuality() > 0) {
					quality = quality + p.getQuality();
					todev++;
				}
			}
		}
		if (nearPoints.size() > 0) {
			return (float) (quality / todev);
		} else {
			return 0;
		}
	}

	public ArrayList<Point> getPoints() {
		return this.points;
	}
}
