package me.jy89hui.MappingStuff;

import java.util.ArrayList;

import me.jy89hui.Interfaces_2DRenderingSystem.Action;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class ActionBuffer {
	/**
	 * This is a object that allows you to store action objects into an array
	 * and then access/use them at a later date.
	 */
	private boolean lookingForCmd = false;
	private ArrayList<Action> actions = new ArrayList<Action>();

	public ActionBuffer(ArrayList<Action> actions) {
		this.actions = actions;
	}

	public ActionBuffer() {

	}

	/**
	 * Make the action buffer perform the first action in the array. While
	 * making sure it is not already in the process of running said command. For
	 * example: You cannot have an action that creates and runs another action
	 */
	public void doNextAction() {
		try {
			if (!this.lookingForCmd) {
				this.lookingForCmd = true;
				this.actions.get(0).go();
				this.actions.remove(0);
				this.lookingForCmd = false;
			}// else {logger.warning("was looking 4 cmd");}

		} catch (IndexOutOfBoundsException e) {
			logger.normalError("Exception at ActionBuffer>doNextAction cmds left: "
					+ this.actions.size());
			e.printStackTrace();
			this.lookingForCmd = false;
		}
	}

	public void addActionToList(Action a) {
		this.actions.add(a);
	}

	public void setBuffer(ArrayList<Action> List) {
		this.actions = List;
	}

	public int getCmdsLeft() {
		return this.actions.size();
	}

	public boolean hasCmdsLeft() {
		if (this.actions.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void fuckingDoTheNextAction() {
		try {
			this.actions.get(0).go();
			this.actions.remove(0);
		} catch (IndexOutOfBoundsException e) {
			logger.warning("Exception at ActionBuffer>doNextAction");
		}
	}

}
