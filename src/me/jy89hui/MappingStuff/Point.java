package me.jy89hui.MappingStuff;

import java.awt.Color;
import java.awt.Graphics;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class Point extends SmartComponent {
	// Ignore SX and SY
	/**
	 * TODO add some color options. EX allow the user to place specific points
	 * and those points would have an unchangeable color.
	 */
	/**
	 * Notes: All calculations for X and Y are then translated into swing cords,
	 * Their locations are stored in the robots cord plane. Size of object may
	 * be pretty important for a good picture
	 */
	private ServerMain mainclass = ServerMain.main;
	private float x;
	private float y;
	private int howManyTimesThePointCanBeHit = 10; // Max value of timesHit
	private int size = 2; // meaning 1cm
	// private int minSize=1; // To avoid becoming invisible -- caused problems
	private int maxQualityRating = 100;
	private double quality = 0.0;
	private boolean forcedQuality = false;
	private int timesHit = 1;

	// LOCATION X AND Y ARE DEFINED IN THE SUPERCLASS OF SMARTCOMPONENT
	/**
	 * Define a simple PointX PointY, this will most likely be the least used
	 * constructor
	 * 
	 * @param x
	 * @param y
	 */

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
		this.quality = 0;
		this.refreshQualityRating();

	}

	public Point(float x, float y) {
		this.x = x;
		this.y = y;
		this.quality = 0;
		this.refreshQualityRating();
	}

	/**
	 * With this constructor you can create a point that has a starting quality,
	 * this will be very useful because you most likely dont want to define a
	 * point with a starting quality of 0, Unless you set the quality then there
	 * will be problems mapping out the points
	 * 
	 * @param x
	 * @param y
	 * @param quality
	 */
	public Point(int x, int y, int timesHit) {
		this.x = x;
		this.y = y;
		this.timesHit = timesHit;
		this.refreshQualityRating();
		mainclass.map.resizingNeeded(this.x, this.y);
	}

	public void setTimesHit(int times) {
		this.timesHit = times;
	}

	/**
	 * Simply print the point, This will most likely be done in the Map class
	 * but for now I decided to let the Point also have a method to print itself
	 */
	@Override
	public void PrintComp(Graphics g) {
		g.setColor(this.getColorBasedOnQuality());
		g.fillRect(
				mainclass.map.pointToSwingLocation(this.x, this.y)[0],
				mainclass.map.pointToSwingLocation(this.x, this.y)[1],
				mainclass.map.applySizeOf1Cm(this.size),
				mainclass.map.applySizeOf1Cm(this.size));

	}

	public double getQuality() {
		return this.quality;
	}

	/**
	 * These 2 methods (below) will allow you to determine what method of
	 * determining color you want and it will allow you to set the max color,
	 * Lets say the max times hit is 5, It will divide your max red by 5 and
	 * multiply it by the times hit, allowing you to alter the colors easily
	 * 
	 * @param r
	 * @param g
	 * @param b
	 * @return
	 */

	public Color getColorBasedOnQuality(int r, int g, int b) {
		Color c = new Color((int) (r / this.maxQualityRating * this.quality),
				(int) (g / this.maxQualityRating * this.quality), (int) (b
						/ this.maxQualityRating * this.quality));
		return c;
	}

	public Color getColorBasedOnTimesHit(int r, int g, int b) {
		Color c = new Color(r / this.howManyTimesThePointCanBeHit
				* this.timesHit, g / this.howManyTimesThePointCanBeHit
				* this.timesHit, b / this.howManyTimesThePointCanBeHit
				* this.timesHit);
		return c;
	}

	/**
	 * These 2 methods on the other hand will go from 0,0,0 to 255,255,255 which
	 * is from black to white and allow you to choose the method of Quality or
	 * times hit.
	 * 
	 * @return
	 */
	public Color getColorBasedOnQuality() {
		int red = (int) (255 / this.maxQualityRating * this.quality);
		int green = (int) (255 / this.maxQualityRating * this.quality);
		int blue = (int) (255 / this.maxQualityRating * this.quality);
		try {
			return new Color(red, green, blue);
		} catch (Exception e) {

			logger.log("Error with color. Tried to create a color with r:"
					+ red + " g:" + green + " b:" + blue + " Quality: "
					+ this.quality);
			return new Color(100, 100, 100);
		}
	}

	public Color getColorBasedOnTimesHit() {
		Color c = new Color(255 / this.howManyTimesThePointCanBeHit
				* this.timesHit, 255 / this.howManyTimesThePointCanBeHit
				* this.timesHit, 255 / this.howManyTimesThePointCanBeHit
				* this.timesHit);
		return c;
	}

	/**
	 * Some comment that I am too tired and lazy to write right now.
	 */
	public void Hit() {
		if (this.timesHit < this.howManyTimesThePointCanBeHit) {
			this.timesHit++;
		}
		this.refreshQualityRating();
		/*
		 * TODO add quality checks
		 */
	}

	/**
	 * A method that will be called when the laser hits a point past this point
	 * but on the same angle.
	 */
	public void Passed() {
		if (this.timesHit > 0) {
			this.timesHit--;
		}
		this.refreshQualityRating();
		/*
		 * TODO add quality checks
		 */
	}

	/**
	 * 	
	 */
	public void refreshQualityRating() {
		if (!this.forcedQuality) {
			double valueOfEachRating = this.maxQualityRating / 100;
			double valueOfTimesHit = valueOfEachRating * 100;
			// double valueOfNearbyPoints=10;
			// double valueOfClientAccuracy = valueOfEachRating * 50;
			valueOfTimesHit = (valueOfTimesHit / this.howManyTimesThePointCanBeHit)
					* this.timesHit;
			this.quality = valueOfTimesHit;
		}

		if (this.quality >= this.maxQualityRating) {
			this.quality = this.maxQualityRating;
		}
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public String toString() {
		// Point/X/Y/TimesHit/ClientAccuracy/Quality
		return "Point/" + this.x + "/" + this.y + "/" + "/" + this.timesHit
				+ "/" + this.quality;
	}

	public int getTimesHit() {
		return this.timesHit;
	}

	@Override
	public int getLocX() {
		return (int) this.x;
	}

	@Override
	public int getLocY() {
		return (int) this.y;
	}

	public float getFloatLocX() {
		return this.x;
	}

	public float getFloatLocY() {
		return this.y;
	}

	public float getAngleFromRobot() {
		double[] distToRobot = this.getDistanceToRobot();
		try {
			float angleToRobot = (float) Math.toDegrees(Math.atan2(
					distToRobot[1], distToRobot[0]));
			while (angleToRobot < 0) {
				angleToRobot = angleToRobot + 360;
			}
			while (angleToRobot > 360) {
				angleToRobot = angleToRobot - 360;
			}
			return angleToRobot;
		} catch (ArithmeticException e) {
			e.printStackTrace();
			// logger.normalError("What the hell... tried to / by 0. Equation: "+distToRbot[0]+"/"+distToRbot[1]);
			return 0;
		}

	}

	public double[] getDistanceToRobot() {
		double[] xydist = {
				(this.x - mainclass.map.robot.getLocationX()),
				(this.y - mainclass.map.robot.getLocationY()) };
		return xydist;
	}

	public double getDistanceToRobotHypotenuse() {
		double[] xydists = this.getDistanceToRobot();
		double dist = Math.sqrt(Math.pow(xydists[0], 2)
				+ Math.pow(xydists[1], 2));
		return dist;
	}
}
