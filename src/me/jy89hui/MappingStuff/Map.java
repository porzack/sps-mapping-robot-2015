package me.jy89hui.MappingStuff;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.MovableComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;
import me.jy89hui.Objects_2DRenderingSystem.ComponentScaler;
import me.jy89hui.Objects_2DRenderingSystem.DragBar;

public class Map extends SmartComponent implements MovableComponent,
		MouseInputListener {
	private ServerMain mainclass = ServerMain.main;
	private DragBar db;
	private ComponentScaler cs;
	private int mouseX = 0;
	private int mouseY = 0;
	private boolean Movable = true;
	private GraphicsMain gMain;
	public boolean careAboutAvgPoints = true;
	private MapToolbar toolbar;
	private ArrayList<Point> points = new ArrayList<Point>();
	public boolean showingPointData = false;
	public Robot robot;
	public static ArrayList<Sector> sectors = new ArrayList<Sector>();
	// This is the constant that signifies how to average range when adding an
	// uncertain point
	private int cmOfRange = 5;

	public double sizeOf1Cm = 500.0; // meaning the translation in size of the
										// object, calculated in %

	public Map(int x, int y, int sx, int sy, boolean movable, GraphicsMain gm) {
		JPanel jp = gm.getPanel();
		this.x = x;
		this.y = y;
		this.sy = sy;
		sx = sx - 150;
		this.sx = sx;
		this.gMain = gm;
		this.Movable = movable;
		this.setName("Map");
		this.toolbar = new MapToolbar(this.x + this.sx - 150, this.y,
				(this.sy), this.gMain);
		if (this.Movable) {
			if (!(this instanceof MovableComponent)) {
				logger.FatalError("Cannot cast SmartComponent MAP to a MovableComponent");
			}
			this.db = new DragBar(this.x, this.y, this.sx, this.sy, this,
					this.gMain);
			this.cs = new ComponentScaler(this.x, this.y, this.sx, this.sy,
					this, this.gMain);
			this.cs.setMinX(600);
			jp.add(this.db);
			jp.add(this.cs);

		}
		jp.add(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		this.robot = new Robot(mainclass);
		this.sectors.add(new Sector(0, 45));
		this.sectors.add(new Sector(45, 90));
		this.sectors.add(new Sector(90, 135));
		this.sectors.add(new Sector(135, 180));
		this.sectors.add(new Sector(180, 225));
		this.sectors.add(new Sector(225, 270));
		this.sectors.add(new Sector(270, 315));
		this.sectors.add(new Sector(315, 360));
		this.Update(x, y, sx, sy);
	}

	public Map(int x, int y, int sx, int sy, boolean movable, GraphicsMain gm,
			ArrayList<Point> points) {
		JPanel jp = gm.getPanel();
		this.x = x;
		this.y = y;
		this.sy = sy;
		sx = sx - 150;
		this.sx = sx;
		this.gMain = gm;
		this.Movable = movable;
		this.setName("Map");
		this.toolbar = new MapToolbar(this.x + this.sx - 150, this.y,
				(this.sy), this.gMain);
		if (this.Movable) {
			if (!(this instanceof MovableComponent)) {
				logger.FatalError("Cannot cast SmartComponent MAP to a MovableComponent");
			}
			this.db = new DragBar(this.x, this.y, this.sx, this.sy, this,
					this.gMain);
			this.cs = new ComponentScaler(this.x, this.y, this.sx, this.sy,
					this, this.gMain);
			this.cs.setMinX(600);
			jp.add(this.db);
			jp.add(this.cs);
		}
		jp.add(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		this.robot = new Robot(mainclass);
		this.sectors.add(new Sector(0, 45));
		this.sectors.add(new Sector(45, 90));
		this.sectors.add(new Sector(90, 135));
		this.sectors.add(new Sector(135, 180));
		this.sectors.add(new Sector(180, 225));
		this.sectors.add(new Sector(225, 270));
		this.sectors.add(new Sector(270, 315));
		this.sectors.add(new Sector(315, 360));
		this.Update(x, y, sx, sy);

	}

	public void addPoints(ArrayList<Point> points) {
		for (Point p : points) {
			this.registerNewPoint(p.getLocX(), p.getLocY(), p.getTimesHit());
		}
	}

	/**
	 * This method will add a new point at a certain location with params X and
	 * Y. those params will then be created into a point. Quality will actually
	 * be averaged with nearby points with quality being slightly more important
	 * than other nearby ones
	 * 
	 * 
	 * TODO add code to delete points that have : - less quality than this point
	 * - A distance that is massive - few nearby points
	 * 
	 * @param x
	 * @param y
	 */
	public void registerNewPoint(int x, int y) {
		boolean foundAPoint = false;
		// 1st: Search through the array to look for similar points
		for (Point p : this.points) {
			if (p.getLocX() == x && p.getLocY() == y) {
				foundAPoint = true;
				p.Hit();
				this.removePointsInWayOfPoint(p);
			}
		}
		if (!foundAPoint) {
			Point point = new Point(x, y);
			this.points.add(point);
			for (Sector s : this.sectors) {
				s.addPoint(point);
			}
			this.resizingNeeded(x, y);

		}
	}

	public void registerNewPoint(float x, float y) {
		boolean foundAPoint = false;
		// 1st: Search through the array to look for similar points
		for (Point p : this.points) {
			float diffX = Math.abs(x - p.getLocX());
			float diffY = Math.abs(y - p.getLocY());
			if (diffX < 2.5 && diffY < 2.5) {
				foundAPoint = true;
				p.Hit();
				this.removePointsInWayOfPoint(p);
			}
		}
		if (!foundAPoint) {
			Point point = new Point(x, y);
			this.points.add(point);
			for (Sector s : this.sectors) {
				s.addPoint(point);
			}
			this.resizingNeeded(x, y);
		}
	}

	public void registerNewPoint(int x, int y, int timesHit) {
		boolean foundAPoint = false;
		// 1st: Search through the array to look for similar points
		for (Point p : this.points) {
			if (p.getLocX() == x && p.getLocY() == y) {
				foundAPoint = true;
				p.Hit();
			}
		}
		if (!foundAPoint) {
			Point point = new Point(x, y, timesHit);
			this.points.add(point);
			for (Sector s : this.sectors) {
				s.addPoint(point);
			}
			this.resizingNeeded(x, y);
		}
	}

	/**
	 * This method will allow you to remove/lower the quality of methods which
	 * are in the path of robot -> Point. This method will be called in
	 * registerNewPoint method and will remove / lower the quality of points on
	 * hit()
	 * 
	 * @param p
	 */
	public void removePointsInWayOfPoint(Point p) {
		int angle = (int) p.getAngleFromRobot();
		for (Point pt : this.points) {
			if ((int) pt.getAngleFromRobot() == angle) {
				if (pt.getDistanceToRobotHypotenuse() < p
						.getDistanceToRobotHypotenuse()) {
					if (pt.getQuality() < p.getQuality()) {
						pt.Passed();
						logger.log("lowered a point at angle "
								+ p.getAngleFromRobot() + " and "
								+ pt.getAngleFromRobot());
					}
				}
			}
		}
	}

	/**
	 * This method simply converts a location in the robots world into a world
	 * that swing can plot
	 * 
	 * @param p
	 * @return
	 */
	public int[] pointToSwingLocation(double x, double y) {
		// we could use applySizeOf1Cm(int x) but that is less accurate than
		// floats
		double nx = (x / 100.0);
		double ny = (y / 100.0);
		nx = nx * this.sizeOf1Cm;
		ny = ny * this.sizeOf1Cm;
		// logger.silentlog("nX: "+nx+" nY: "+ny);
		int totalX = this.sx;
		int totalY = this.sy;
		nx = nx + (totalX / 2) + this.x;
		ny = ny + (totalY / 2) + this.y;
		// logger.silentlog("nnX: "+nx+" nnY: "+ny);
		int[] toReturn = { (int) nx, (int) ny };
		return toReturn;

	}

	/**
	 * This method converts the size of an object into the correct sizing
	 * 
	 * @param x
	 * @return
	 */
	public int applySizeOf1Cm(int x) {
		// add 0.6 to force round up the int
		return (int) Math.round(((x / 100.0) * this.sizeOf1Cm));
	}

	public float applySizeOf1Cm(float x) {
		return (float) ((x / 100.0) * this.sizeOf1Cm);
	}

	public double applySizeOf1Cm(double x) {
		return ((x / 100.0) * this.sizeOf1Cm);
	}

	// XY meaning the objects current location, we will not resize until some
	// object is outside the frame
	public void resizingNeeded(float x1, float y1) {
		int outsideArea = 10;
		int x = this.pointToSwingLocation(x1, y1)[0];
		int y = this.pointToSwingLocation(x1, y1)[1];
		boolean invalid = false;
		if ((this.x + this.sx) < x + outsideArea) {
			invalid = true;
		} else if ((this.y + this.sy) < y + outsideArea) {
			invalid = true;
		} else if (this.x > x - outsideArea) {
			invalid = true;
		} else if (this.y > y - outsideArea) {
			invalid = true;
		} else {
			return;
		}
		while (invalid) {
			this.sizeOf1Cm = this.sizeOf1Cm - 0.5; // 0.5 is how much we go down
													// each time
			x = this.pointToSwingLocation(x1, y1)[0];
			y = this.pointToSwingLocation(x1, y1)[1];
			invalid = false;
			if ((this.x + this.sx) < x) {
				invalid = true;
			} else if ((this.y + this.sy) < y) {
				invalid = true;
			} else if (this.x > x) {
				invalid = true;
			} else if (this.y > y) {
				invalid = true;
			} else {
				return;
			}
		}

	}

	@Override
	public void onCommand(Command c) {
		if (c.getArgs()[1].equalsIgnoreCase("SetSize")) {
			try {
				this.sx = Integer.parseInt(c.getArgs()[2]);
				this.sy = Integer.parseInt(c.getArgs()[3]);
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName()
						+ " Setsize <SizeXInt> <SizeYInt>");
			}
		}
		if (c.getArgs()[1].equalsIgnoreCase("map")) {
			try {
				mapper.testMapping();
				c.Handled();
			} catch (Exception e) {

			}
		} else if (c.getArgs()[1].equalsIgnoreCase("SetLocation")) {
			try {
				this.TranslateLocation(
						this.x - Integer.parseInt(c.getArgs()[2]), this.y
								- Integer.parseInt(c.getArgs()[3]));
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName()
						+ " SetLocation <LocXInt> <LocYInt>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("setname")) {
			try {
				this.setName(c.getArgs()[2]);
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName() + " Setname <NameString>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("SomeMenuCommand")) {
			try {
				logger.log("PROGRESS");
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName() + " Something... ");
			}
		} else {

		}
	}

	public ArrayList<Point> getPoints() {
		return this.points;
	}

	public void setPoints(ArrayList<Point> points) {
		this.points = points;
	}

	@Override
	public void PrintComp(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(new Color(0, 0, 0));
		g2d.fillRoundRect(this.x, this.y, this.sx, this.sy, 10, 10);
		this.toolbar.PrintComp(g);
		for (int x = 0; x < this.getPoints().size(); x++) {
			this.getPoints().get(x).PrintComp(g);
		}
		if (this.Movable) {
			this.db.PrintComp(g);
			this.cs.PrintComp(g);
		}
		this.robot.PrintComp(g);
		// Do the hover stuff with points
		this.pointDetails(g);

	}

	public void pointDetails(Graphics g) {
		if (this.showingPointData) {
			for (Point p : this.points) {
				int xx = this.pointToSwingLocation(p.getLocX(), p.getLocY())[0];
				int yy = this.pointToSwingLocation(p.getLocX(), p.getLocY())[1];
				int disX = Math.abs(this.mouseX - xx);
				int disY = Math.abs(this.mouseY - yy);
				if (disX < 10 && disY < 10) {
					int x = this.mouseX + 20;
					int y = this.mouseY + 20;
					g.setColor(new Color(10, 10, 10));
					g.fillRoundRect(x, y, 150, 100, 10, 10);
					g.setColor(new Color(90, 90, 255));
					g.drawRoundRect(x, y, 150, 100, 10, 10);
					g.setColor(new Color(100, 150, 250));
					g.drawString("x:" + p.getLocX(), x + 10, y + 15);
					g.drawString("y:" + p.getLocY(), x + 70, y + 15);
					g.drawString("Quality:" + (int) (p.getQuality()), x + 10,
							y + 30);
					g.drawString("TimesHit:" + p.getTimesHit(), x + 10, y + 45);
					g.drawString("Angle:" + p.getAngleFromRobot(), x + 10,
							y + 60);
					Color color = p.getColorBasedOnQuality();
					g.drawString(
							"Color: [" + color.getRed() + ","
									+ color.getGreen() + "," + color.getBlue()
									+ "]", x + 10, y + 90);
				}
			}
		}
	}

	@Override
	public void TranslateLocation(int dx, int dy) {
		this.x = this.x - dx;
		this.y = this.y - dy;
		this.Update(this.x, this.y, this.sx, this.sy);
	}

	@Override
	public void TranslateSize(int sx, int sy) {
		this.sx = this.sx - sx;
		this.sy = this.sy - sy;
		this.Update(this.x, this.y, this.sx, this.sy);
	}

	public void Update(int x, int y, int sx, int sy) {
		this.x = x; // because of the toolbar

		this.y = y;
		this.sy = sy;
		this.sx = sx;
		this.setBounds(this.x, this.y, this.sx, this.sy);

		this.toolbar.Update(this.x + this.sx, this.y, (this.sy));
		if (this.Movable) {
			this.db.Update(this.x, this.y, this.sx + 150, this.sy);
			this.cs.Update(this.x, this.y, this.sx + 150, this.sy);
		}
	}

	public int numberOfPoints() {
		return this.getPoints().size();
	}

	public double getAvgQuality() {
		long total = 0;
		for (Point p : this.getPoints()) {
			total = (long) (total + p.getQuality());
		}
		try {
			return (total / this.getPoints().size());
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		this.mouseX = arg0.getX();
		this.mouseY = arg0.getY();
	}

}
