package me.jy89hui.MappingStuff;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.LoadMapStuff.specialPoint;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class Robot extends SmartComponent { // This is the server side idea of
											// the robot.
	private double radius = 11.0;
	/**
	 * Here we set the basic locations, We start them at 0,0 Because that is the
	 * center of the coordinate plane
	 * 
	 *
	 */
	private ServerMain mainclass;
	private float x = 0, y = 0;
	public boolean logCommands = false;
	private int[] startingLocation = { 0, 0 }; // X, Y
	public int[] currentLocation = { 0, 0 }; // X,Y <-- Not used ATM
	// TODO: Change the rotations to doubles if necessary because I am loosing
	// accuracy
	public float currentTotalRotation = 0;// in degrees 0 Means forward
	public float currentLaserRotation = 0;// in degrees
	public float currentBaseRotation = 0;// in degrees
	// How quality works:
	// Quality is simply the range that is acceptable and
	// that we can be sure that the real distance is inside that quality range
	private float fakeAccuracy = 0;
	public float currentQuality = 2; // measured in Cm
	public boolean currentlyPerformingAnAction = false;
	public int cmdsLeft = 0;
	private double radianToDegreeMultiplier = 360 / (2 * Math.PI);
	private double differenceOfDegreesForLaserAndBase = 0;
	private int timesSinceLastSpecialPt = 0;
	private ArrayList<specialPoint> specialPointArry = new ArrayList<specialPoint>();
	private ActionBuffer actionBuffer = new ActionBuffer();

	public Robot(ServerMain mclass) {
		this.mainclass = mclass;
		this.sx = 20;
		this.sy = 20;
		this.addSpecialPoint();
	}

	public float getLocationX() {
		return this.x;
	}

	public float getLocationY() {
		return this.y;
	}

	@Override
	public void PrintComp(Graphics g) {
		ArrayList<specialPoint> antiCocurrentModificationArray = this.specialPointArry;
		for (specialPoint sp : antiCocurrentModificationArray) {
			sp.PrintComp(g);
		}
		g.setColor(new Color(255, 70, 70));
		// Begin drawng the robot
		int MinSize = 10;
		// reduce clutter and calculations by defining the variables up here
		// c stands for current.
		// ex: current x = cx
		int cx = mainclass.map.pointToSwingLocation(this.x, this.y)[0];
		int cy = mainclass.map.pointToSwingLocation(this.x, this.y)[1];
		int cRadius = (int) mainclass.map.applySizeOf1Cm(this.radius); // We
																					// can
																					// accept
																					// imperfections
																					// in
																					// the
																					// drawing.
		// Begin drawing the starting pos line
		g.setColor(new Color(255, 0, 0));
		int lengthOfLinesOnCrossStartingPos = this.sx * 1; // How long the lines
															// are for the
															// starting location
															// cross
		g.drawLine(
				mainclass.map.pointToSwingLocation(
						this.startingLocation[0]
								+ lengthOfLinesOnCrossStartingPos, 0)[0],
				mainclass.map.pointToSwingLocation(
						this.startingLocation[0]
								+ lengthOfLinesOnCrossStartingPos, 0)[1],
				mainclass.map.pointToSwingLocation(
						this.startingLocation[0]
								- lengthOfLinesOnCrossStartingPos, 0)[0],
				mainclass.map.pointToSwingLocation(
						this.startingLocation[0]
								- lengthOfLinesOnCrossStartingPos, 0)[1]);

		g.drawLine(mainclass.map.pointToSwingLocation(0,
				this.startingLocation[1] + lengthOfLinesOnCrossStartingPos)[0],
				mainclass.map.pointToSwingLocation(0,
						this.startingLocation[1]
								+ lengthOfLinesOnCrossStartingPos)[1],
				mainclass.map.pointToSwingLocation(0,
						this.startingLocation[1]
								- lengthOfLinesOnCrossStartingPos)[0],
				mainclass.map.pointToSwingLocation(0,
						this.startingLocation[1]
								- lengthOfLinesOnCrossStartingPos)[1]);
		// Make sure its at least the min size
		g.setColor(new Color(255, 50, 50));
		if (cRadius < MinSize) {
			cRadius = MinSize;
		}
		// draw the robot
		g.fillOval(cx - (cRadius / 2), cy - (cRadius / 2), cRadius, cRadius);
		// Draw the scanner rotation-line
		g.setColor(new Color(50, 50, 255));
		int lengthOfDirectionalLine = mainclass.map
				.applySizeOf1Cm(25);
		g.drawLine(
				cx,
				cy,
				mainclass.map.pointToSwingLocation(
						(int) (this.x + (Math.cos(this.currentLaserRotation
								/ this.radianToDegreeMultiplier) * lengthOfDirectionalLine)),
						(int) (this.y + (Math.sin(this.currentLaserRotation
								/ this.radianToDegreeMultiplier) * lengthOfDirectionalLine)))[0],
				mainclass.map.pointToSwingLocation(
						(int) (this.x + (Math.cos(this.currentLaserRotation
								/ this.radianToDegreeMultiplier) * lengthOfDirectionalLine)),
						(int) (this.y + (Math.sin(this.currentLaserRotation
								/ this.radianToDegreeMultiplier) * lengthOfDirectionalLine)))[1]);
		// Draw the base rotation
		g.setColor(new Color(50, 255, 50));
		int lengthOfDirectionalLineBase = mainclass.map
				.applySizeOf1Cm(25);
		g.drawLine(
				cx,
				cy,
				mainclass.map.pointToSwingLocation(
						(int) (this.x + (Math.cos(this.currentBaseRotation
								/ this.radianToDegreeMultiplier) * lengthOfDirectionalLineBase)),
						(int) (this.y + (Math.sin(this.currentBaseRotation
								/ this.radianToDegreeMultiplier) * lengthOfDirectionalLineBase)))[0],
				mainclass.map.pointToSwingLocation(
						(int) (this.x + (Math.cos(this.currentBaseRotation
								/ this.radianToDegreeMultiplier) * lengthOfDirectionalLineBase)),
						(int) (this.y + (Math.sin(this.currentBaseRotation
								/ this.radianToDegreeMultiplier) * lengthOfDirectionalLineBase)))[1]);

		/*
		 * int x2 = (int) (this.x + (Math.cos(this.currentLaserRotation /
		 * this.radianToDegreeMultiplier) * 300)); int y2 = (int) (this.y +
		 * (Math.sin(this.currentLaserRotation / this.radianToDegreeMultiplier)
		 * * 300)); mainclass.map.registerNewPoint(x2, y2, (float)
		 * (Math.random() * 90) + 10);
		 */
	}

	public int[] getStartingLoc() {
		return this.startingLocation;
	}

	public int[] getCurrentLoc() {
		return this.currentLocation;
	}

	/**
	 * These are a set of methods that allow the server to request certain
	 * actions from the client. Most of these are executed as requests instead
	 * of commands though. For example if server told client to move 15 cm but
	 * the client can only move in 0.9 cm increments then the client will move
	 * 14.4 steps and stop. It will then report back that it moved 14.4 cm and
	 * wont even mention the 15cm request. The server will then try to be
	 * compliant and not request farther movement (because it is not possible
	 * for the client to reach xy pos). This will also help when trying to avoid
	 * nearby objects, lets say there is a wall 8cm away then it could move back
	 * 2cm and the server would have to deal with it instead of the server
	 * managing object collision.
	 * 
	 */

	public void move(float distanceInCM) { // Returns:
											// "Client/moved/float distanceMovedInCM/boolean stillMoving/int numberOfCmdsRemaining"
		ServerClass.sendToClient("Server/Move/" + distanceInCM);

	}

	// Returns:
	// "Client/robotAngleAt/float currentDegrees/boolean stillTurning/int numberOfCmdsRemaining"
	public void turnRelative(float degrees) {
		float degs = degrees;
		ServerClass.sendToClient("Server/RelativeBaseTurn/" + degs);
	}

	public void turnToAbsolute(float degrees) {
		float degs = degrees;
		ServerClass.sendToClient("Server/AbsoluteBaseTurn/" + degs);
	}

	// Returns: Client/scannerAngleAt/float currentdegreesXy/float z/boolean
	// stillTurning/int
	// numberOfCmdsRemaining/$"
	public void scannerTurnRelative(float degrees) {
		float degs = degrees;
		ServerClass.sendToClient("Server/RelativeScannerTurn/" + degs);
	}

	public void scannerTurnToAbsolute(float degrees) {
		float degs = degrees;
		ServerClass.sendToClient("Server/AbsoluteScannerTurn/" + degs);
	}

	public void scannerElevateRelative(float degrees) {
		float degs = degrees;
		ServerClass.sendToClient("Server/ScannerElevationChange/" + degs);
	}

	public void scannerElevateToAbsolute(float degrees) {
		float degs = degrees;
		ServerClass.sendToClient("Server/ScannerElevationAbsolute/" + degs);
	}

	// Returns:
	// "Client/NewDataPoint/float currentX/float currentY/ float baseAngle/ float elevation/ float distance/ float quality/Boolean stillscanning/ int numberOfCmdsRemaining"
	public void scan() {
		ServerClass.sendToClient("Server/Scan");
	}

	public void scanWithQuality(float quality) {
		float qualityt = quality;
		ServerClass.sendToClient("Server/ScanWithQuality/" + qualityt);

	}

	public void scanFromTo(float startAngle, float endAngle,
			float incrementInDegrees) {// Returns:

		float start = startAngle;
		float end = endAngle;
		float inc = incrementInDegrees;
		ServerClass.sendToClient("Server/ScanFromTo/" + start + "/" + end + "/"
				+ inc);

	}

	// Client/Robot/scanQualitySetTo/float Quality/$int numberOfCmdsRemaining/"
	public void scanSetQuality(float cmOfAccuracy) { // Returns:
		float acc = cmOfAccuracy;
		ServerClass.sendToClient("Server/SetScanAccuracy/" + acc);
	}

	// Client/StatusReport/Float locx/ float locy/ float baseAngle/ float
	// elevation/float batteryVoltage/ float Quality/boolean
	// laserPointerEnabled/int numberOfCmdsRemaining"
	public void giveStatus() {// Returns:
		// ServerClass.sendToClient("Server/RequestStatus");
		logger.normalError("Please dont use giveStatus. It is currently broken");
	}

	// returns "Client/LaserPointer/boolean enabled/int numberOfCmdsRemaining/$"
	public void setScannerLaserPointerStatus(int status) {
		// Status values:
		// 0: Default switching (clients control)
		// 1: Override On
		// 2: Override Off
		ServerClass.sendToClient("Server/SetScannerLaserPointerStatus/"
				+ status);
		// logger.normalError("Please dont use setScannerLaserPointerStatus. It is currently broken");

	}

	public void killRun() { // Will return status message before death.
		ServerClass.sendToClient("Server/KillRun");
	}

	// returns: Client/testConnection/numberOfCmdsRemaining/$
	public void testConnection() {
		ServerClass.sendToClient("Server/testConnection");
	}

	// returns: Client/
	/**
	 * This method calibrates how far each movement is Basically: - Takes
	 * distance from 0 - Moves +100 steps - Takes distance from 0 - Generate a
	 * new distance-steps ratio value
	 */
	public void Calibrate() {
		ServerClass.sendToClient("Server/RefereshDistancetoStepsRatio");

	}

	public void recalculateSectors() {
		for (Sector sect : mainclass.map.sectors) {
			sect.setPoints(mainclass.map.getPoints());
		}
	}

	/**
	 * This is a very important method that will parse incoming strings from the
	 * client. This method can take outside sources (meaning not the
	 * Client-server socket connection) but that is not recommended. Whenever a
	 * message is received from the client it is then sent here so it can be
	 * parsed.
	 * 
	 * @param s
	 */
	public void parseResult(String s) {
		/**
		 * Facts: Must begin with Client Must be separated by slashes / Must
		 * have a numberOfCmdsRemaining The data is split into datasets using
		 * the / The second dataset must state the action You can have infinite
		 * datasets as long as it starts with Client and ends with
		 * numberOfCmdsRemaining The server holds clients location and current
		 * rotation although the client will also keep track of these numbers.
		 * You must have a boolean at the 2nd to last dataset. This boolean
		 * signifies if that is the last package detailing a specific action.
		 */
		if (s.equals("")) {
			return;
		}
		String[] data = s.split("/");
		// This is just the position where the action is stored
		int actionPos = 1;
		// Position where data starts
		int dataStart = 2;
		int dataEnd = data.length - 1; // position where data ends
		int numOfData = dataEnd - dataStart; // How much data is in each reading

		if (!data[0].equalsIgnoreCase("Client")) {
			logger.normalError("Cannot parse result, did not start with Client");
			return;
		}
		// Set number of cmds left
		try {
			this.cmdsLeft = Integer.parseInt(data[dataEnd]);
		} catch (Exception e) {
			e.printStackTrace();
			logger.normalError("Error Parsing the number of commands left");
			return;
		}
		try {
			this.currentlyPerformingAnAction = Boolean
					.parseBoolean(data[dataEnd - 1]);
		} catch (Exception e) {
			e.printStackTrace();
			logger.normalError("Error Parsing CurrentlyPerformingAnAction.");
			return;
		}

		if (data[actionPos].equalsIgnoreCase("Moved")) {
			if (this.logCommands)
				logger.silentlog("Moved " + Float.parseFloat(data[dataStart]));
			/*
			 * That is simple math that translates the robots position based on
			 * its current direction and how much distance is applied.
			 */
			this.x = (float) (this.x + (Math.cos(this.currentBaseRotation
					/ this.radianToDegreeMultiplier) * Float
					.parseFloat(data[dataStart])));
			this.y = (float) (this.y + (Math.sin(this.currentBaseRotation
					/ this.radianToDegreeMultiplier) * Float
					.parseFloat(data[dataStart])));
			this.addSpecialPoint();
			this.recalculateSectors();
			mainclass.map.resizingNeeded(this.x, this.y);

		} else if (data[actionPos].equalsIgnoreCase("baseAngleAt")) {
			float angle = Float.parseFloat(data[dataStart]);
			if (this.logCommands)
				logger.silentlog("Robot angle is currently at "
						+ Float.parseFloat(data[dataStart]));
			double diffForLaser = (angle - this.currentBaseRotation);
			this.currentBaseRotation = angle;
			this.currentLaserRotation = (float) (this.currentLaserRotation + diffForLaser);
			this.differenceOfDegreesForLaserAndBase = this.differenceOfDegreesForLaserAndBase
					+ diffForLaser;
			this.refreshRotations();

		} else if (data[actionPos].equalsIgnoreCase("scannerAngleAt")) {
			if (this.logCommands)
				logger.silentlog("Scanner angle is currently at "
						+ Float.parseFloat(data[dataStart]));
			this.currentLaserRotation = Float.parseFloat(data[dataStart]);
			this.currentLaserRotation = (float) (this.currentLaserRotation + this.differenceOfDegreesForLaserAndBase);

			this.refreshRotations();

		} else if (data[actionPos].equalsIgnoreCase("newDataPoint")) {
			Float distance = Float.parseFloat(data[dataStart]);
			if (distance < 300) {
				float x1 = (float) (this.x + (Math
						.cos(this.currentLaserRotation
								/ this.radianToDegreeMultiplier) * distance));
				float y1 = (float) (this.y + (Math
						.sin(this.currentLaserRotation
								/ this.radianToDegreeMultiplier) * distance));
				// logger.log("New Data. LocX: "+x1+" LocY: "+y1+" Distance: "+distance+" Scanner Angle: "+this.currentLaserRotation,
				// new Color(9,112,11));
				if (this.logCommands)
					logger.silentlog("New point registered at: " + x1 + ", "
							+ y1);
				mainclass.map.registerNewPoint(x1, y1);
			}

		} else if (data[actionPos].equalsIgnoreCase("newDataPointWithQuality")) {
			Float distance = Float.parseFloat(data[dataStart]);
			Float Quality = Float.parseFloat(data[dataStart + 1]);
			int x1 = (int) Math.round(this.x
					+ (Math.cos(this.currentLaserRotation
							/ this.radianToDegreeMultiplier) * distance));
			int y1 = (int) Math.round(this.y
					+ (Math.sin(this.currentLaserRotation
							/ this.radianToDegreeMultiplier) * distance));
			if (this.logCommands)
				logger.silentlog("New point registered at: " + x1 + ", " + y1);
			mainclass.map.registerNewPoint(x1, y1);

		} else if (data[actionPos].equalsIgnoreCase("scanQualitySetTo")) {
			if (this.logCommands)
				logger.silentlog("Quality is currently at "
						+ Integer.parseInt(data[dataStart]));
			this.currentQuality = Float.parseFloat(data[dataStart]);

		} else if (data[actionPos].equalsIgnoreCase("StatusReport")) {
			// This will come after I figure out everything

		} else if (data[actionPos].equalsIgnoreCase("LaserPointer")) {
			/*
			 * I don't really think this method should return anything but I
			 * will keep the if here in case I do ever decide to add a return
			 * mechanism
			 */
		} else if (data[actionPos].equalsIgnoreCase("testConnection")) {
			logger.log("Connection works! ", new Color(0, 255, 0));
			ServerClass.sendToClient("Connection Works!");

		} else if (data[actionPos].equalsIgnoreCase("sendMessage")) {
			logger.log("Client has sent: " + data[dataStart], new Color(20,
					200, 20));
		} else {
			logger.FatalError("Failed reading a result from client, Cannot determine what action is needed in order to handle the incoming data");
		}

		tryToRunNextCmd();
	}

	/**
	 * This method will determine if all commands are done, if so: Do next cmd
	 */
	public void tryToRunNextCmd() {
		if (!this.currentlyPerformingAnAction) {
			this.actionBuffer.doNextAction();
		}
	}

	public void addSpecialPoint() {
		this.timesSinceLastSpecialPt++;
		if (this.timesSinceLastSpecialPt > 10) {
			this.specialPointArry.add(new specialPoint(this.x, this.y));
		}
	}

	public ActionBuffer getActionBuffer() {
		return this.actionBuffer;
	}

	public void setActionBuffer(ActionBuffer ab) {
		this.actionBuffer = ab;
	}

	/**
	 * This method is designed to convert the angles down to angles of 360. If
	 * an angle is say.. 361 then it will become 1. Or say 362.. it will become
	 * 2.
	 */
	public void refreshRotations() {
		this.currentTotalRotation = this.currentBaseRotation
				+ this.currentLaserRotation;
		while (this.currentTotalRotation > 360) {
			this.currentTotalRotation = this.currentTotalRotation - 360;
		}
		while (this.currentBaseRotation > 360) {
			this.currentBaseRotation = this.currentBaseRotation - 360;
		}
		while (this.currentLaserRotation > 360) {
			this.currentLaserRotation = this.currentLaserRotation - 360;
		}
	}

	public ArrayList<specialPoint> getSpecialPoints() {
		return this.specialPointArry;
	}

	public void setSpecialPoints(ArrayList<specialPoint> points) {
		this.specialPointArry = points;
	}
}
