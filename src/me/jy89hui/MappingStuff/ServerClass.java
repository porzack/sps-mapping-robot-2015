package me.jy89hui.MappingStuff;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

import me.jy89hui.AMain.ServerMain;
import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Program;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class ServerClass extends GraphicsMain implements Runnable, Program { 

	/**
	 * Runs the server.
	 */
	private static ServerMain mainclass = ServerMain.main;
	public boolean sent = true;
	public String Message;
	public static ServerClass dataServer = new ServerClass();
	public static Thread dataThread = (new Thread(dataServer));
	public static ArrayList<Socket> clientsConnected = new ArrayList<Socket>();
	private static ServerSocket ConnectionListener;
	public static Socket socket;
	public static Sender sender = new Sender();
	public static Listener listener = new Listener();
	public static int portNumber;
	public static boolean started=false;

	public static void StartServer() throws IOException {
		if (started) return;
		started=true;
		// Random r = new Random(); was used w /
		portNumber = 4555;
		InetAddress locIP = InetAddress.getByName("10.0.0.38");
		ConnectionListener = new ServerSocket(portNumber, 0, locIP);
		ConnectionListener.setReuseAddress(true);
		logger.log("--------------------------------------------", new Color(
				50, 50, 200), new Font("Century", Font.BOLD, 15));
		logger.log("Server has started up with port: " + portNumber, new Color(
				80, 80, 15), new Font("Century", Font.BOLD, 15));
		logger.log("Server IP: " + locIP.toString(), new Color(80, 80, 15),
				new Font("Century", Font.BOLD, 15));
		logger.log("--------------------------------------------", new Color(
				50, 50, 200), new Font("Century", Font.BOLD, 15));
		// ServerSocket listener = new ServerSocket(Number);
		dataServer.requestCmds(Integer.MAX_VALUE);
		dataThread.start();

	}

	public void requestCmds(int cmdCount) {
		mainclass.Console.RequestNextCommands(this, cmdCount);
	}

	@Override
	public void run() {
		try {
			while (true) {
				this.socket = ConnectionListener.accept();
				clientsConnected.add(socket);
				/*
				 * Run the listener because I am lazy and really dont care about
				 * accepting multiple clients. This program will only accept 1
				 * client because I am not creating separate threads for each
				 * listener
				 */
				logger.log("Client has connected with IP: "
						+ socket.getInetAddress().toString());
				(new Thread(listener)).start();
				try {
					PrintWriter out = new PrintWriter(socket.getOutputStream(),
							true);
					out.println(new Date().toString());

				} finally {
					// socket.close(); Not helpful, Bad
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionListener.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public ServerSocket getSocket() {
		return ConnectionListener;
	}

	@Override
	public String getName() {
		return "Server";

	}

	public static int getPort() {
		try {
			return portNumber;
		} catch (NullPointerException e) {
			return 0;
		}
	}

	@Override
	public void Load() {
		// TODO Auto-generated method stub

	}

	public static void sendToClient(String s) {
		Sender.send(s);

	}

	@Override
	public void giveData(String text) {
		logger.log("Sending [" + text + "] to clients");
		sender.send(text);
	}

	@Override
	public void Run() {
		this.run(); // <-- no idea when the Run() method would be called but im
					// just going to route it to run().

	}

}

class Listener implements Runnable {
	private BufferedReader input;
	private ServerMain mainclass = ServerMain.main;

	@Override
	public void run() {
		try {
			this.input = new BufferedReader(new InputStreamReader(
					ServerClass.socket.getInputStream()));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		while (true) {
			try {
				String message = this.input.readLine();
				logger.silentlog("Client has sent [" + message + "]");
				mainclass.map.robot.parseResult(message);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
	}
}

class Sender {
	public static void send(String string) {
		for (int x = 0; x < ServerClass.clientsConnected.size(); x++) {
			try {
				PrintWriter out = new PrintWriter(ServerClass.clientsConnected
						.get(x).getOutputStream(), true);
				out.println(string + "\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		logger.silentlog("Sent: [" + string + "] to clients");
	}
}
