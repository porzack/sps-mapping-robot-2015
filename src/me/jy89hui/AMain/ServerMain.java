package me.jy89hui.AMain;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JPanel;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.LoadMapStuff.Reader;
import me.jy89hui.ManagingSystems_2DRenderingSystem.CommandManager;
import me.jy89hui.ManagingSystems_2DRenderingSystem.PaintManager;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;
import me.jy89hui.MappingStuff.Map;
import me.jy89hui.MappingStuff.ServerClass;
import me.jy89hui.Objects_2DRenderingSystem.ConsoleLogger;
import me.jy89hui.Objects_2DRenderingSystem.Menu;
import me.jy89hui.Objects_2DRenderingSystem.MenuBox;

public class ServerMain extends GraphicsMain {
	public static ServerMain main = (new ServerMain());
	public static JPanel Panel = new PaintManager();
	public static CommandManager comandmanager = new ComandControl();
	public static PaintManager paintManager;
	public static Map map;



	public static void main(String[] args){
		//logger.FatalError("PLEASE DO NOT USE THE MAIN2D CLASS FROM JAVA2DRENDERINGSYSTEM.");
		main.setEqual();
		main.refreshRate=50;
		//main.RefreshTimerDelay();
		main.version="3.8";
		main.getFrame().setName("ServerManager");
		logger.log("Running Version: "+main.version, new Color(20,75,50) , new Font("Times New Roman", Font.PLAIN, 20));
		Panel.setLayout(null);
		paintManager = new PaintManager();
		paintManager.load(main);
		LoadFrame(0, 0, 2000, 1175, Panel, main);

		main.LoadAcceptedComponents();
		log("Program Started.");

		getFrame().repaint();
		
	}
	public void setEqual(){
		this.main=this;
	}



	public void LoadAcceptedComponents() {
		// this.RealComponents.add(new BallFrame(650,50,500,500,this));
		ArrayList<MenuBox> boxes = new ArrayList<MenuBox>();
		boxes.add(new MenuBox("Create New Room Map") {
			@Override
			public void clicked() {
				main.menu.Destroy();
				main.Console = new ConsoleLogger(25, 50, 600,
						300, main, comandmanager);
				main.map = new Map(0, 0,
						getFrame().getSize().width,
						getFrame().getSize().height - 24, false,
						main);
				main.RealComponents.add(main.map);
				main.RealComponents
						.add(main.Console);
				main.getFrame().add(main.Console);
				main.getPanel().add(main.Console);
				try {
					ServerClass.StartServer();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		boxes.add(new MenuBox("Load a Room Map") {
			@Override
			public void clicked() {
				main.menu.Destroy();
				main.RealComponents.add(new Reader(0, 0,
						getFrame().getSize().width,
						getFrame().getSize().height - 24));
			}
		});
		this.menu = new Menu(0, 0, getFrame().getSize().width, getFrame()
				.getSize().height, false, this, boxes);

		this.RealComponents.add(this.menu);

	}
	public void addMapAndConsole(){
		main.Console = new ConsoleLogger(25, 50, 600,
				300, main, comandmanager);
		main.map = new Map(0, 0,
				getFrame().getSize().width,
				getFrame().getSize().height - 24, false,
				main);
		main.RealComponents.add(main.map);
		main.RealComponents
				.add(main.Console);
		main.getFrame().add(main.Console);
		main.getPanel().add(main.Console);
	}
}
